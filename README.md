# Meteo

Ceci est un exercice pour un recrutement.

## Consignes de l'exercice
Réalisation d'une application web de météo en VueJS.

Possibilité de chercher la météo d'une ville ;

Vue détaillée de la météo (Actuelle, sur plusieurs jours, toutes les infos que vous pouvez trouver);

Possibilité de rajouter des villes aux favoris et avoir accès à la météo de ces villes;
Utilisation de l'API de ton choix (nombreuses en ligne gratuitement);

### Les contraintes sont les suivantes
Utilisation de VueJS ;

Pour les appels, utilisation d'axios ET de vue-router.

Pour le css, je vous laisse choisir ce que vous souhaitez. Vous devez toutefois faire votre propre css.

Vous pouvez également implémenter un module de traduction anglais/francais.


## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Run End-to-End Tests with [Cypress](https://www.cypress.io/)

```sh
npm run build
npm run test:e2e # or `npm run test:e2e:ci` for headless testing
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
