export const datetimeFormats = {
  'en-US': {
    short: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
    },
    long: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      weekday: 'short',
      hour: 'numeric',
      minute: 'numeric',
    },
    longWeekday: {
      weekday: 'short',
      day: 'numeric',
    },
    time: {
      hour: '2-digit',
      minute: '2-digit',
    },
    hour: {
      hour: '2-digit',
    },
  },
  'fr-FR': {
    short: {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
    },
    long: {
      year: 'numeric',
      month: 'long',
      day: '2-digit',
    },
    longWeekday: {
      weekday: 'short',
      day: '2-digit',
    },
    time: {
      hour: '2-digit',
      minute: '2-digit',
    },
    hour: {
      hour: '2-digit',
    },
  },
}
export const numberFormats = {
  'fr-FR': {
    percent: {
      style: 'percent',
      maximumFractionDigits: 2,
    },
    oneDecimal: {
      style: 'decimal',
      maximumFractionDigits: 1,
    },
  },
  'en-US': {
    percent: {
      style: 'percent',
      maximumFractionDigits: 2,
    },
    oneDecimal: {
      style: 'decimal',
      maximumFractionDigits: 1,
    },
  },
}
