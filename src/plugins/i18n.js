import { createI18n } from 'vue-i18n'
import localeFr from './../../locales/fr.json'
import localeEn from './../../locales/en.json'
import { getKeyByValue } from '@/utils/common.utils.js'
import { datetimeFormats, numberFormats } from '@/plugins/i18n.utils.js'
import { LOCALIZATION } from '@/plugins/i18n.const.js'

const defaultLocale = LOCALIZATION.fr

const locales = {
  [LOCALIZATION.fr]: localeFr,
  [LOCALIZATION.en]: localeEn,
}

export const i18n = createI18n({
  locale: defaultLocale,
  fallbackLocale: defaultLocale,
  messages: locales,
  datetimeFormats,
  numberFormats,
})

export const getLocalIndex = (i18n) => {
  const locale = getKeyByValue(LOCALIZATION, i18n.locale)
  if (!locale)
  {return 'fr'}

  return locale
}
