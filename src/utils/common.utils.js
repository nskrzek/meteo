export const groupByKey = (list, key, valueFunction) => list.reduce((grouped, item) => {
  const value = typeof key === 'function'
    ? key(item)
    : item[key]

  if (!grouped[value]) {
    grouped[value] = []
  }
  const row = valueFunction
    ? valueFunction(item)
    : item

  grouped[value].push(row)

  return grouped
}, {})

export const mapObject = (object, callback) => Object
  .fromEntries(Object.entries(object).map(([key, value]) => [key, callback(value)]))

export const capitalizeFirstLetter = (string) => string ? string.charAt(0).toUpperCase() + string.slice(1) : ''

export const getKeyByValue = (object, value) => Object.keys(object).find(key => object[key] === value)
