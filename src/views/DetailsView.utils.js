import {
  groupByKey,
  mapObject,
  capitalizeFirstLetter,
} from '@/utils/common.utils.js'

export const formatForecastWeather = (forecastWeather) => {
  const list = forecastWeather.map(line => {
    const date = line.dt_txt.split(' ')
    return {
      ...line,
      day: date[0],
      hour: date[1],
    }
  })

  const groupedByDate = groupByKey(list, 'day')

  return mapObject(groupedByDate, day => {
    const middleDay = day[Math.round(day.length / 2)]
    const weather = middleDay.weather[0]
    return {
      resume: {
        date: middleDay.day,
        description: capitalizeFirstLetter(weather.description),
        icon: weather.icon,
        min: Math.min(...day.map(l => l.main.temp_min)),
        max: Math.max(...day.map(l => l.main.temp_max)),
        temp: day.reduce((sum, { main }) => sum + main.temp, 0) / day.length,
      },
      details: [...day],
    }
  })
}
