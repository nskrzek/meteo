import { defineStore } from 'pinia'

export const useCitiesStore = defineStore({
  id: 'cities',
  state: () => ({
    cities: [],
  }),
  getters: {
    items: (state) => state.cities,
  },
  actions: {
    addCity(name) {
      this.cities.push(name)
    },
    removeCity(name) {
      const index = this.cities.findIndex(city => city === name)
      if(index >= 0) {
        this.cities.splice(index, 1)
      }
    },
  },
  persist: {
    enabled: true,
    strategies: [{
      key: 'citiesStore',
    }],
  },
})
