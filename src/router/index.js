import { createRouter, createWebHistory } from 'vue-router'

import { i18n } from '@/plugins/i18n'
import { LOCALIZATION } from '@/plugins/i18n.const.js'

import HomeView from '@/views/HomeView.vue'
import DetailsView from '@/views/DetailsView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/:locale?',
      children: [
        {
          path: '',
          name: 'home',
          component: HomeView,
        },
        {
          path: 'details/:query',
          name: 'details',
          component: DetailsView,
        },
      ],
    },
  ],
})

router.beforeEach((to, from, next) => {
  const locale = to.params.locale

  if (!LOCALIZATION[locale])
  {return next('fr')}

  if (i18n.global.locale !== LOCALIZATION[locale]) {
    i18n.global.locale = LOCALIZATION[locale]
  }

  return next()
})

export { router }
