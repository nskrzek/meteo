/* eslint-env node */
module.exports = {
  root: true,
  'extends': [
    'plugin:vue/vue3-recommended',
    'eslint:recommended',
    'plugin:import/recommended',
  ],
  overrides: [
    {
      files: [
        'cypress/e2e/**.{cy,spec}.{js,ts,jsx,tsx}',
      ],
      'extends': [
        'plugin:cypress/recommended',
      ],
    },
    {
      files: ['*.vue'],
      rules: {
        indent: 'off',
        'import/no-default-export': 'off',
      },
    },
  ],
  ignorePatterns: ['**/*.config.js'],
  parserOptions: {
    ecmaVersion: 'latest',
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['@', './src'],
        ],
        extensions: ['.ts', '.js', '.jsx'],
      },
    },
  },
  rules: {
    'comma-dangle': ['warn', 'always-multiline'],
    curly: ['warn', 'all'],
    quotes: ['warn', 'single'],
    semi: ['warn', 'never'],
    indent: ['warn', 2, { SwitchCase: 1 }],
    'no-underscore-dangle': 'off',
    'max-len': 'off',
    'vue/max-len': [
      'error',
      {
        code: 180,
        template: 180,
        tabWidth: 2,
        comments: 180,
        ignorePattern: '',
        ignoreComments: false,
        ignoreTrailingComments: false,
        ignoreUrls: false,
        ignoreStrings: false,
        ignoreTemplateLiterals: false,
        ignoreRegExpLiterals: false,
        ignoreHTMLAttributeValues: false,
        ignoreHTMLTextContents: false,
      },
    ],
    'import/prefer-default-export': 'off',
    'import/no-default-export': 'error',
    'vue/attribute-hyphenation': 'off',
  },
}
